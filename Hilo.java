package pack;

public class Hilo extends Thread{
	private static int cont = 0;	// Recordemos que si una variable es estatica, existe solo una vez en toda la clase
									// es decir, es la misma, compartida por todos los objetos existentes de la clase.
									// no habra diferentes valores de la variable segun el objeto y si un objeto modifica su
	public Hilo () {}				// variable, la modifica para todos.
	
	public void run()
	{
		for(int i = 0; i < 1000; i++)	// Habiendo 1000 hilos deberia dar 1.000.000 sin embargo esto no sucede por el indeterminismo.
		{								// Como se utilizo join, es seguro que se hicieron 1.000.000 de iteraciones de este bucle
			cont++;						// pero el resultado no sera 1.000.000 porque cuando dos o mas hilos estan escribiendo una misma
		}								// variable compartida a la vez, el valor de esta es indeterminado. Existe una gran posibilidad
	}									// de que mientras un hilo esta escribiendo la variable, otro tambien escriba. Luego el "valor 
										// anterior" a partir del cual el segundo hilo escribe es incorrecto, pues el primer hilo todavia
	public int getCont()				// no termino de escribir. Con lo cual la escritura del primer hilo se pierde y queda la del segundo
	{									// que fue obtenida a partir del "valor inicial" con el que pretendia escribir tambien el primer hilo.
		return cont;
	}
}