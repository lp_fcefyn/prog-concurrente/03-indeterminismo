package pack;

public class Principal {
	
	public static void main (String[] args)
	{
		Hilo vec[] = new Hilo[1000];
		
		for(int i = 0; i < vec.length; i++)
		{
			vec[i] = new Hilo();
			vec[i].start();
		}																							
		
		try {										// Sin el join, el resultado se aleja mucho del deseado pues se puede imprimir el mensaje			
			for(int i = 0; i < vec.length; i++)		// cuando todavia los hilos no terminaron sus calculos.
			{										
				vec[i].join();			
			}																				
		}catch(Exception ex) {}							
		
		System.out.println("La cuenta es "+ vec[0].getCont()+" o "+vec[500].getCont());
	}
	
}