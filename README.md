# 03-Indeterminismo

Indeterminismo: Falta de causalidad, es decir, los acontecimientos no dependen de un proceso causal lineal (como serían las instrucciones de un programa no paralelo ni concurrente en el que se ejecutan las instrucciones una después de otra). 